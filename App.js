import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import {Text, View} from 'react-native';
import Imagezoom from './src/pages/tugas3/Imagezoom';
import ImplementasiLibrary from './src/pages/ImplementasiLibrary';

const Stack = createNativeStackNavigator();
// const Tab = createMaterialBottomTabNavigator();

function SettingsScreen() {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>Settings!</Text>
    </View>
  );
}

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Implementasi"
        screenOptions={{headerShown: false}}>
        {/* <Stack.Screen name="SignIn" component={SignIn} /> */}
        {/* <Stack.Screen name="SignUp" component={SignUp} /> */}
        {/* <Stack.Screen name="ResetPass" component={ResetPass} /> */}
        <Stack.Screen name="Implementasi" component={ImplementasiLibrary} />
        <Stack.Screen name="Imagezoom" component={Imagezoom} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
